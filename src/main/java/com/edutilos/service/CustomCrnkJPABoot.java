package com.edutilos.service;

import com.edutilos.util.CrnkJPA;
import io.crnk.core.engine.properties.NullPropertiesProvider;
import io.crnk.core.engine.transaction.TransactionRunner;
import io.crnk.jpa.JpaModule;
import io.crnk.jpa.JpaRepositoryConfig;
import io.crnk.jpa.internal.JpaResourceInformationProvider;
import io.crnk.rs.CrnkFeature;

import javax.enterprise.inject.Model;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.EntityType;

/**
 * Created by Nijat Aghayev on 21.02.19.
 */
//@Model
public class CustomCrnkJPABoot {
    @PersistenceContext(unitName = "sprint_dashboard")
    private EntityManager emSprintDashboard;

    private static final TransactionRunner tr = new JPATransationRunner();

    public void setup(CrnkFeature feature) {
        feature.addModule(createCrnkJPAEntityManager("idta", emSprintDashboard));
    }

    private JpaModule createCrnkJPAEntityManager(String unitName, EntityManager em) {
        JpaModule jpaModule = JpaModule.newServerModule(em, tr);
        jpaModule.setResourceInformationProvider(new JpaResourceInformationProvider(new NullPropertiesProvider()) {
            @Override
            public boolean accept(Class<?> resourceClass) {
                CrnkJPA crnkJPA = resourceClass.getAnnotation(CrnkJPA.class);
                if (crnkJPA != null && crnkJPA.value().equals(unitName)) {
                    return super.accept(resourceClass);
                }
                return false;
            }
        });
        for (EntityType<?> entityType : em.getMetamodel().getEntities()) {
            Class<?> repoClass = entityType.getJavaType();
            CrnkJPA crnkJPA = repoClass.getAnnotation(CrnkJPA.class);
            if (crnkJPA != null && crnkJPA.value().equals(unitName)) {
                jpaModule.addRepository(JpaRepositoryConfig.create(repoClass));
            }
        }
        return jpaModule;
    }
}
