package com.edutilos.service;

import com.edutilos.util.DateUtil;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.crnk.core.boot.CrnkProperties;
import io.crnk.core.queryspec.mapper.DefaultQuerySpecUrlMapper;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

/**
 * Created by Nijat Aghayev on 21.02.19.
 */
@Provider
public class CustomCrnkFeature extends io.crnk.rs.CrnkFeature {

    @Inject
    private CustomCrnkJPABoot jpaBoot;

    public static final Long DEFAULT_LIMIT = 25L;

    @Override
    public boolean configure(FeatureContext context) {
        io.crnk.rs.CrnkFeature feature = new io.crnk.rs.CrnkFeature();
        DefaultQuerySpecUrlMapper querySpecDeserializer = (DefaultQuerySpecUrlMapper) feature.getUrlMapper();
        querySpecDeserializer.setAllowUnknownAttributes(true);
        SimpleDateFormat sdfFull = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
        feature.getObjectMapper().registerModule(new JavaTimeModule());
        feature.getObjectMapper().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        feature.getObjectMapper().setDateFormat(sdfFull);
        feature.getBoot().getModuleRegistry().getTypeParser().addParser(Date.class, input -> {
            try {
                return sdfFull.parse(input);
            } catch (Exception ignore) {
                try {
                    return sdf.parse(input);
                } catch (Exception ignore2) {
                    try {
                        return DateUtil.getSdfYyyymmdd().parse(input);
                    } catch (Exception e) {
                        //ignore
                    }
                }
            }
            return null;
        });

        jpaBoot.setup(feature);

        String version = null;
        String environment = System.getProperty("environment", "production");
        if ("production".equals(environment)) {
            try (InputStream is = getClass().getResourceAsStream("/version.properties")) {
                Properties p = new Properties();
                if (is != null) {
                    p.load(is);
                    version = p.getProperty("version", null);
                }
            } catch (IOException | IllegalArgumentException e) {
                LoggerFactory.getLogger(getClass()).debug("Cannot read pom.properties", e);
            }
        }

        if (version != null) {
            version = "/" + version;
        }

        context.property(CrnkProperties.WEB_PATH_PREFIX, version)
                .property(CrnkProperties.RESOURCE_DEFAULT_DOMAIN, "")
                .property(CrnkProperties.RETURN_404_ON_NULL, "true")
                .register(feature);

        return true;
    }
}
