package com.edutilos.service;

import io.crnk.core.engine.transaction.TransactionRunner;

import javax.transaction.Transactional;
import java.util.concurrent.Callable;

/**
 * Created by Nijat Aghayev on 21.02.19.
 */
public class JPATransationRunner implements TransactionRunner {
    @Transactional
    @Override
    public <T> T doInTransaction(Callable<T> callable) {
        try {
            return callable.call();
        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                throw (RuntimeException)e;
            }
            throw new RuntimeException(e);
        }
    }
}