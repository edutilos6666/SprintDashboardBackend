package com.edutilos.service;

import io.crnk.legacy.locator.JsonServiceLocator;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.ws.rs.ext.Provider;

/**
 * Created by Nijat Aghayev on 21.02.19.
 */
public class WildflyServiceLocator implements JsonServiceLocator {

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getInstance(Class<T> aClass) {
        BeanManager beanManager = CDI.current().getBeanManager();
        Bean<?> bean = beanManager.resolve(beanManager.getBeans(aClass));
        CreationalContext<?> creationalContext = beanManager.createCreationalContext(bean);

        return (T) beanManager.getReference(bean, aClass, creationalContext);
    }

}
