package com.edutilos.model;

import io.crnk.core.resource.annotations.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
@Getter
@Setter
@Entity
@Table(name="test_teacher")
@JsonApiResource(type="test-teacher")
public class TestTeacher implements Serializable {
    @Id @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonApiId
    private Long id;
    @Column(name="NAME")
    private String name;
    @Column(name="UNIVERSITY_ID")
    private Long universityId;

    @JsonApiRelation(serialize = SerializeType.ONLY_ID)
    @ManyToOne
    @JoinColumn(name = "UNIVERSITY_ID", referencedColumnName = "ID",
    insertable = false, updatable = false)
    private TestUniversity testUniversity;

    @Transient
    @JsonApiRelation(serialize = SerializeType.ONLY_ID, lookUp = LookupIncludeBehavior.AUTOMATICALLY_WHEN_NULL)
    private Set<TestStudent> testStudents;

    @Override
    public String toString() {
        try {
            return "TestTeacher{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", universityId=" + universityId +
                    ", testUniversity=" + testUniversity.getName() +
                    ", testStudents=" + testStudents.stream().map(TestStudent::getName).collect(Collectors.toList()).toString() +
                    '}';
        } catch(NullPointerException ex) {
            return "TestTeacher{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", universityId=" + universityId +
                    '}';
        }
    }


    public TestTeacher(String name, Long universityId) {
        this.name = name;
        this.universityId = universityId;
    }

    public TestTeacher() {
    }
}
