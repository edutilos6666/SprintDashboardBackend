package com.edutilos.model;

import lombok.Data;

/**
 * Created by Nijat Aghayev on 19.02.19.
 */
@Data
public class Worker {
    private long id;
    private String name;
    private int age;
    private double wage;

    public Worker(long id, String name, int age, double wage) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.wage = wage;
    }

    public Worker() {
    }

}