package com.edutilos.model;

import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Nijat Aghayev on 22.02.19.
 */
@Entity
@Table(name = "test_person")
@Data
@JsonApiResource(type = "test-person")
public class TestPerson implements Serializable {
    @Id
    @JsonApiId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "AGE")
    private Integer age;
    @Column(name = "WAGE")
    private Double wage;
    @Column(name = "ACTIVE")
    private Boolean active;

    public TestPerson(Long id, String name, Integer age, Double wage, Boolean active) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.wage = wage;
        this.active = active;
    }


    public TestPerson(String name, Integer age, Double wage, Boolean active) {
        this.name = name;
        this.age = age;
        this.wage = wage;
        this.active = active;
    }

    public TestPerson() {
    }
}
