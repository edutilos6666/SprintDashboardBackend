package com.edutilos.model;

import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
@Data
@Entity
@Table(name="test_student_teacher")
public class TestStudentTeacher implements Serializable{
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="STUDENT_ID")
    private Long studentId;
    @Column(name="TEACHER_ID")
    private Long teacherId;

    public TestStudentTeacher(Long studentId, Long teacherId) {
        this.studentId = studentId;
        this.teacherId = teacherId;
    }

    public TestStudentTeacher() {
    }
}
