package com.edutilos.model;

import io.crnk.core.resource.annotations.*;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
@Data
@Entity
@Table(name="test_student")
@JsonApiResource(type = "test-student")
public class TestStudent implements Serializable {
    @Id @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonApiId
    private Long id;
    @Column(name= "NAME")
    private String name;
    @Column(name= "UNIVERSITY_ID")
    private Long universityId;
//    @Column(name= "TEACHER_ID")
//    private Long teacherId;

    @JsonApiRelation(serialize = SerializeType.ONLY_ID)
    @ManyToOne
    @JoinColumn(name = "UNIVERSITY_ID", referencedColumnName = "ID",
    insertable = false, updatable = false)
    private TestUniversity testUniversity;

    @JsonApiRelation(serialize = SerializeType.LAZY, lookUp = LookupIncludeBehavior.NONE)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "test_student_teacher",
            joinColumns = @JoinColumn(name = "STUDENT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "TEACHER_ID", referencedColumnName = "ID"))
    private Set<TestTeacher> testTeachers;


    public TestStudent(String name, Long universityId, Set<TestTeacher> testTeachers) {
        this.name = name;
        this.universityId = universityId;
        this.testTeachers = testTeachers;
    }

    public TestStudent(String name, Long universityId) {
        this.name = name;
        this.universityId = universityId;
    }

    public TestStudent() {
    }
}
