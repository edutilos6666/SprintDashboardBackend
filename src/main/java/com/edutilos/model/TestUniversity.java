package com.edutilos.model;

import io.crnk.core.resource.annotations.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
@Getter
@Setter
@Entity
@Table(name="test_university")
@JsonApiResource(type="test-university")
public class TestUniversity implements Serializable {
    @Id @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonApiId
    private Long id;
    @Column(name="NAME")
    private String name;
    @Column(name="COUNTRY")
    private String country;
    @Column(name="CITY")
    private String city;
    @Column(name="PLZ")
    private String plz;

    @Transient
    @JsonApiRelation(serialize = SerializeType.ONLY_ID, lookUp =  LookupIncludeBehavior.AUTOMATICALLY_WHEN_NULL)
    private Set<TestStudent> testStudents;
    @Transient
    @JsonApiRelation(serialize = SerializeType.ONLY_ID, lookUp = LookupIncludeBehavior.AUTOMATICALLY_WHEN_NULL)
    private Set<TestTeacher> testTeachers;

    @Override
    public String toString() {
        try {
            return "TestUniversity{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", country='" + country + '\'' +
                    ", city='" + city + '\'' +
                    ", plz='" + plz + '\'' +
                    ", testStudents=" + testStudents.stream().map(TestStudent::getName).collect(Collectors.toList()).toString() +
                    ", testTeachers=" + testTeachers.stream().map(TestTeacher::getName).collect(Collectors.toList()).toString() +
                    '}';
        } catch(NullPointerException ex) {
            return "TestUniversity{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", country='" + country + '\'' +
                    ", city='" + city + '\'' +
                    ", plz='" + plz + '\'' +
                    '}';
        }
    }


    public TestUniversity(String name, String country, String city, String plz) {
        this.name = name;
        this.country = country;
        this.city = city;
        this.plz = plz;
    }

    public TestUniversity() {
    }
}
