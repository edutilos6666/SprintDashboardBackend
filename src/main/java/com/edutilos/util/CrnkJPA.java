package com.edutilos.util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * created by  Nijat Aghayev on 22.02.19
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CrnkJPA {
    String value();
}
