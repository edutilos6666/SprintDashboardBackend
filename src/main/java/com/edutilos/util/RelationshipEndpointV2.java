package com.edutilos.util;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.resource.list.DefaultResourceList;
import io.crnk.core.resource.list.ResourceList;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by Nijat Aghayev on 22.02.19.
 */
public abstract class RelationshipEndpointV2<T, I extends Serializable, D, J extends Serializable> implements io.crnk.core.repository.RelationshipRepositoryV2<T, I , D, J> {
    private Class<T> sourceEntityClass;
    private Class<D> targetEntityClass;

    @SuppressWarnings("unchecked")
    public RelationshipEndpointV2() {
        Type t = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        Type tt = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[2];
        try {
            sourceEntityClass = (Class<T>) Class.forName(t.toString().split(" ")[1]);
            targetEntityClass = (Class<D>) Class.forName(tt.toString().split(" ")[1]);
        } catch (ClassNotFoundException ignore) {
        }
    }

    @Override
    public Class<T> getSourceResourceClass() {
        return sourceEntityClass;
    }

    @Override
    public Class<D> getTargetResourceClass() {
        return targetEntityClass;
    }

    @Override
    public void setRelation(T t, J j, String s) {

    }

    @Override
    public void setRelations(T t, Iterable<J> iterable, String s) {

    }

    @Override
    public void addRelations(T t, Iterable<J> iterable, String s) {

    }

    @Override
    public void removeRelations(T t, Iterable<J> iterable, String s) {

    }

    public abstract D findOne(I i, String s, QuerySpec querySpec);

    @Override
    public D findOneTarget(I i, String s, QuerySpec querySpec) {
        return findOne(i, s, querySpec);
    }

    public abstract List<D> findMany(I i, String s, QuerySpec querySpec);

    @Override
    public ResourceList<D> findManyTargets(I i, String s, QuerySpec querySpec) {
        List<D> ret = findMany(i, s, querySpec);
        if (ret instanceof ResourceList) {
            return (ResourceList<D>) ret;
        }
        if (ret == null) {
            ret = Collections.emptyList();
        }
        return new DefaultResourceList<>(ret, null, null);
    }
}
