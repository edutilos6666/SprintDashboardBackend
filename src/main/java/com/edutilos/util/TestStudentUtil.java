package com.edutilos.util;

import com.edutilos.dao.TestStudentTeacherDAO;
import com.edutilos.dao.TestTeacherDAO;
import com.edutilos.dao.TestUniversityDAO;
import com.edutilos.model.TestStudent;
import com.edutilos.model.TestStudentTeacher;
import com.edutilos.model.TestTeacher;
import com.edutilos.model.TestUniversity;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestStudentUtil implements EntityDependency<TestStudent> {
    @Inject
    private TestUniversityDAO testUniversityDAO;
    @Inject
    private TestTeacherDAO testTeacherDAO;
    @Inject
    private TestStudentTeacherDAO testStudentTeacherDAO;

    @Override
    public TestStudent applyDependency(TestStudent entity) {
        final Long universityId = entity.getUniversityId();
        if(universityId != null) {
            TestUniversity testUniversity = testUniversityDAO.findById(universityId);
            if(testUniversity != null)
                entity.setTestUniversity(testUniversity);
        }
        List<TestStudentTeacher> testStudentTeachers = testStudentTeacherDAO.findByStudentId(entity.getId());
        Set<TestTeacher> testTeachers = testStudentTeachers.stream().map(one-> testTeacherDAO.findById(one.getTeacherId())).collect(Collectors.toSet());
        entity.setTestTeachers(testTeachers);
        return entity;
    }
}
