package com.edutilos.util;

import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public interface EntityDependency<T> {

    default List<T> applyDependencies(List<T> list) {
        if (list != null) {
            for (T curEntity : list) {
                applyDependency(curEntity);
            }
        }
        return list;
    }

    T applyDependency(T entity);


}
