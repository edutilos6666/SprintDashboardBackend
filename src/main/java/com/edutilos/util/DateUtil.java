package com.edutilos.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.MonthDay;

/**
 * Created by Nijat Aghayev on 21.02.19.
 */
public class DateUtil {

    public static DateFormat getSdfYyyymmdd() {
        return new SimpleDateFormat("yyyyMMdd");
    }
}
