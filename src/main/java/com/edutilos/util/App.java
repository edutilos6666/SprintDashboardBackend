package com.edutilos.util;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Nijat Aghayev on 22.02.19.
 */
//@ApplicationScoped
@ApplicationPath("/")
public class App extends Application {
    // this seems to be usefull
    // actually it really is, as the Application starts here ;)
}