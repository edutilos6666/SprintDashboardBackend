package com.edutilos.util;

import io.crnk.client.CrnkClient;
import io.crnk.client.http.HttpAdapterProvider;
import io.crnk.client.http.apache.HttpClientAdapter;
import io.crnk.client.http.apache.HttpClientAdapterProvider;
import io.crnk.core.queryspec.mapper.DefaultQuerySpecUrlMapper;

/**
 * Created by Nijat Aghayev on 22.02.19.
 */
public class APIClient extends CrnkClient {

    public APIClient() {
        this(System.getProperty("API_URL", "http://localhost:8080/SprintDashboardBackend-1.0-SNAPSHOT/"));
    }

    public APIClient(String host) {
        super(host);
        if (getUrlMapper() instanceof DefaultQuerySpecUrlMapper) {
            ((DefaultQuerySpecUrlMapper) getUrlMapper()).setAllowUnknownAttributes(true);
        }
        initialize();
    }

    @Override
    public void registerHttpAdapterProvider(HttpAdapterProvider httpAdapterProvider) {
        if (httpAdapterProvider instanceof HttpClientAdapterProvider) {
            super.registerHttpAdapterProvider(httpAdapterProvider);
        }
    }

    private void initialize() {
        HttpClientAdapter adapter = (HttpClientAdapter) getHttpAdapter();
//        adapter.addListener(builder -> builder.setDefaultHeaders(AuthService.getDefaultHeader()));
    }
}
