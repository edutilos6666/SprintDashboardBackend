package com.edutilos.util;

import com.edutilos.dao.TestUniversityDAO;
import com.edutilos.model.TestTeacher;
import com.edutilos.model.TestUniversity;

import javax.inject.Inject;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestTeacherUtil implements EntityDependency<TestTeacher> {
    @Inject
    private TestUniversityDAO testUniversityDAO;

    @Override
    public TestTeacher applyDependency(TestTeacher entity) {
        Long testUniversityId = entity.getUniversityId();
        if(testUniversityId != null) {
            TestUniversity testUniversity = testUniversityDAO.findById(testUniversityId);
            if(testUniversity != null)
                entity.setTestUniversity(testUniversity);
        }
        return entity;
    }
}
