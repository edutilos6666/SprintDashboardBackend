package com.edutilos.endpoint;

import com.edutilos.dao.TestTeacherDAO;
import com.edutilos.model.TestTeacher;
import com.edutilos.util.APIEndpointV2;
import com.edutilos.util.TestTeacherUtil;
import io.crnk.core.queryspec.QuerySpec;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestTeacherEndpoint<S extends TestTeacher> extends APIEndpointV2<TestTeacher, Long> {

    @Inject
    private TestTeacherDAO testTeacherDAO;
    @Inject
    private TestTeacherUtil testTeacherUtil;

    @Override
    public TestTeacher findByID(Long id) {
        TestTeacher one = testTeacherDAO.findById(id);
        one = testTeacherUtil.applyDependency(one);
        return one;
    }

    @Override
    public List<TestTeacher> findBy(QuerySpec querySpec) {
        List<TestTeacher> all = testTeacherDAO.findAll();
        all.forEach(one -> testTeacherUtil.applyDependency(one));
        return all;
    }

    @Override
    public void delete(Long id) {
        testTeacherDAO.delete(id);
    }

    @Override
    public <S extends TestTeacher> S create(S s) {
        testTeacherDAO.insert(s);
        return (S)findByID(s.getId());
    }

    @Override
    public <S extends TestTeacher> S save(S s) {
        return (S)testTeacherDAO.update(s);
    }
}
