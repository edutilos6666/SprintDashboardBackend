package com.edutilos.endpoint;

import com.edutilos.dao.TestPersonDAO;
import com.edutilos.model.TestPerson;
import com.edutilos.util.APIEndpointV2;
import io.crnk.core.queryspec.QuerySpec;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by Nijat Aghayev on 22.02.19.
 */
@Dependent
public class TestPersonEndpoint<S extends TestPerson> extends APIEndpointV2<TestPerson, Long> {

    @Inject
    private TestPersonDAO dao;

    @Override
    public TestPerson findByID(Long id) {
        return dao.findById(id);
    }

    @Override
    public List<TestPerson> findBy(QuerySpec querySpec) {
        Optional<String> name = getFilterParam(querySpec, "name");
        Optional<String> minAge = getFilterParam(querySpec, "minAge");
        Optional<String> maxAge = getFilterParam(querySpec, "maxAge");
        Optional<String> minWage = getFilterParam(querySpec, "minWage");
        Optional<String> maxWage = getFilterParam(querySpec, "maxWage");
        Optional<Boolean> active = getFilterParam(querySpec, "active");
        if(name.isPresent())
            return dao.findByName(name.get());
        if(minAge.isPresent() && maxAge.isPresent())
            return dao.findByAgeBetween(Integer.valueOf(minAge.get()),
                    Integer.valueOf(maxAge.get()), true);
        if(minWage.isPresent() && maxWage.isPresent())
            return dao.findByWageBetween(Double.valueOf(minWage.get()),
                    Double.valueOf(maxWage.get()), true);
        if(active.isPresent())
            return dao.findByActive(active.get());
        return dao.findAll();
    }

    @Override
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    public <S extends TestPerson> S create(S s) {
        dao.insert(s);
        return (S)findByID(s.getId());
    }

    @Override
    public <S extends TestPerson> S save(S s) {
        return (S)dao.update(s);
    }
}
