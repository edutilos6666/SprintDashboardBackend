package com.edutilos.endpoint;

import com.edutilos.dao.TestUniversityDAO;
import com.edutilos.model.TestUniversity;
import com.edutilos.util.APIEndpointV2;
import io.crnk.core.queryspec.QuerySpec;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestUniversityEndpoint<S extends TestUniversity> extends APIEndpointV2<TestUniversity, Long> {
    @Inject
    private TestUniversityDAO testUniversityDAO;

    @Override
    public TestUniversity findByID(Long id) {
        return testUniversityDAO.findById(id);
    }

    @Override
    public List<TestUniversity> findBy(QuerySpec querySpec) {
        return testUniversityDAO.findAll();
    }

    @Override
    public void delete(Long id) {
        testUniversityDAO.delete(id);
    }

    @Override
    public <S extends TestUniversity> S create(S s) {
        testUniversityDAO.insert(s);
        return (S)findByID(s.getId());
    }

    @Override
    public <S extends TestUniversity> S save(S s) {
        return (S)testUniversityDAO.update(s);
    }
}
