package com.edutilos.endpoint;

import com.edutilos.dao.TestStudentDAO;
import com.edutilos.dao.TestStudentTeacherDAO;
import com.edutilos.dao.TestTeacherDAO;
import com.edutilos.dao.TestUniversityDAO;
import com.edutilos.model.TestStudent;
import com.edutilos.model.TestStudentTeacher;
import com.edutilos.model.TestTeacher;
import com.edutilos.util.APIEndpointV2;
import com.edutilos.util.TestStudentUtil;
import io.crnk.core.queryspec.QuerySpec;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestStudentEndpoint<S extends TestStudent> extends APIEndpointV2<TestStudent, Long> {
    @Inject
    private TestStudentDAO testStudentDAO;
    @Inject
    private TestStudentTeacherDAO testStudentTeacherDAO;
    @Inject
    private TestStudentUtil testStudentUtil;

    @Override
    public TestStudent findByID(Long id) {
        TestStudent one = testStudentDAO.findById(id);
//        one = testStudentUtil.applyDependency(one);
        return one;
    }

    @Override
    public List<TestStudent> findBy(QuerySpec querySpec) {
        List<TestStudent> all = testStudentDAO.findAll();
//        return all.stream().map(one-> testStudentUtil.applyDependency(one)).collect(Collectors.toList());
        return all;
    }

    @Override
    public void delete(Long id) {
        testStudentDAO.delete(id);
    }

    @Override
    public <S extends TestStudent> S create(S s) {
        testStudentDAO.insert(s);
//        Set<TestTeacher> testTeachers =  s.getTestTeachers();
//        try {
//            testTeachers.forEach(one -> {
//                testStudentTeacherDAO.insert(new TestStudentTeacher(s.getId(), one.getId()));
//            });
//        } catch(NullPointerException ex) {
//
//        }
        return (S)findByID(s.getId());
    }

    @Override
    public <S extends TestStudent> S save(S s) {
        S ret = (S)testStudentDAO.update(s);
//        Set<TestTeacher> testTeachers = ret.getTestTeachers();
//        testStudentTeacherDAO.deleteByStudentId(ret.getId());
//        try {
//            testTeachers.forEach(one -> {
//                testStudentTeacherDAO.insert(new TestStudentTeacher(ret.getId(), one.getId()));
//            });
//        } catch(NullPointerException ex) {
//
//        }
        return ret;
    }
}
