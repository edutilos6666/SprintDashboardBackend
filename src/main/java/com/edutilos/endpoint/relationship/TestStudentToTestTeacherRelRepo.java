package com.edutilos.endpoint.relationship;

import com.edutilos.dao.TestStudentDAO;
import com.edutilos.dao.TestStudentTeacherDAO;
import com.edutilos.dao.TestTeacherDAO;
import com.edutilos.model.TestStudent;
import com.edutilos.model.TestStudentTeacher;
import com.edutilos.model.TestTeacher;
import com.edutilos.util.RelationshipEndpointV2;
import io.crnk.core.queryspec.QuerySpec;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestStudentToTestTeacherRelRepo extends RelationshipEndpointV2<TestStudent, Long, TestTeacher, Long> {
    @Inject
    private TestStudentDAO testStudentDAO;
    @Inject
    private TestStudentTeacherDAO testStudentTeacherDAO;
    @Inject
    private TestTeacherDAO testTeacherDAO;

    @Override
    public TestTeacher findOne(Long id, String s, QuerySpec querySpec) {
        return null;
    }

    @Override
    public List<TestTeacher> findMany(Long id, String s, QuerySpec querySpec) {
        TestStudent one = testStudentDAO.findById(id);
        if(one == null) return null;
        List<TestStudentTeacher> all =  testStudentTeacherDAO.findByStudentId(one.getId());
        return all.stream().map(tst -> testTeacherDAO.findById(tst.getTeacherId())).collect(Collectors.toList());
    }
}
