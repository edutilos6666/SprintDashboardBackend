package com.edutilos.endpoint.relationship;

import com.edutilos.dao.TestStudentDAO;
import com.edutilos.model.TestStudent;
import com.edutilos.model.TestUniversity;
import com.edutilos.util.RelationshipEndpointV2;
import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.RelationshipRepositoryBase;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestStudentToTestUniversityRelRepo extends RelationshipEndpointV2<TestStudent, Long, TestUniversity, Long> {

    @Inject
    private TestStudentDAO testStudentDAO;

    @Override
    public TestUniversity findOne(Long id, String s, QuerySpec querySpec) {
        TestStudent one = testStudentDAO.findById(id);
        return one.getTestUniversity();
    }

    @Override
    public List<TestUniversity> findMany(Long id, String s, QuerySpec querySpec) {
        return null;
    }
}
