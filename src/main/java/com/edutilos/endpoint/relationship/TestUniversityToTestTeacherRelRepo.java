package com.edutilos.endpoint.relationship;

import com.edutilos.model.TestTeacher;
import com.edutilos.model.TestUniversity;
import com.edutilos.util.RelationshipEndpointV2;
import io.crnk.core.queryspec.QuerySpec;

import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestUniversityToTestTeacherRelRepo extends RelationshipEndpointV2<TestUniversity, Long, TestTeacher, Long> {
    @Override
    public TestTeacher findOne(Long aLong, String s, QuerySpec querySpec) {
        return null;
    }

    @Override
    public List<TestTeacher> findMany(Long aLong, String s, QuerySpec querySpec) {
        return null;
    }
}
