package com.edutilos.endpoint.relationship;

import com.edutilos.dao.TestTeacherDAO;
import com.edutilos.model.TestTeacher;
import com.edutilos.model.TestUniversity;
import com.edutilos.util.RelationshipEndpointV2;
import io.crnk.core.queryspec.QuerySpec;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestTeacherToTestUniversityRelRepo extends RelationshipEndpointV2<TestTeacher, Long, TestUniversity, Long> {

    @Inject
    private TestTeacherDAO testTeacherDAO;

    @Override
    public TestUniversity findOne(Long id, String s, QuerySpec querySpec) {
        TestTeacher one = testTeacherDAO.findById(id);
        return one.getTestUniversity();
    }

    @Override
    public List<TestUniversity> findMany(Long id, String s, QuerySpec querySpec) {
        return null;
    }
}
