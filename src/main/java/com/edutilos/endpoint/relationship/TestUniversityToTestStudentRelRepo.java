package com.edutilos.endpoint.relationship;

import com.edutilos.model.TestStudent;
import com.edutilos.model.TestUniversity;
import com.edutilos.util.RelationshipEndpointV2;
import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.RelationshipRepositoryBase;
import io.crnk.core.repository.foward.ForwardingRelationshipRepository;

import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestUniversityToTestStudentRelRepo extends RelationshipEndpointV2<TestUniversity, Long, TestStudent, Long> {
    @Override
    public TestStudent findOne(Long id, String s, QuerySpec querySpec) {
        return null;
    }

    @Override
    public List<TestStudent> findMany(Long id, String s, QuerySpec querySpec) {
        return null;
    }
}
