package com.edutilos.endpoint.relationship;

import com.edutilos.model.TestStudent;
import com.edutilos.model.TestTeacher;
import com.edutilos.util.RelationshipEndpointV2;
import io.crnk.core.queryspec.QuerySpec;

import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestTeacherToTestStudentRelRepo extends RelationshipEndpointV2<TestTeacher, Long, TestStudent, Long> {
    @Override
    public TestStudent findOne(Long id, String s, QuerySpec querySpec) {
        return null;
    }

    @Override
    public List<TestStudent> findMany(Long id, String s, QuerySpec querySpec) {
        return null;
    }
}
