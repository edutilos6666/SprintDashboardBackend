package com.edutilos.consumer;

import com.edutilos.model.TestPerson;
import com.edutilos.util.APIClient;
import io.crnk.core.queryspec.FilterOperator;
import io.crnk.core.queryspec.FilterSpec;
import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * Created by Nijat Aghayev on 22.02.19.
 */
public class TestPersonConsumer {
    public static void main(String[] args) {
        TestPersonConsumer consumer = new TestPersonConsumer();
        consumer.doRun();
    }

    private final Logger logger = LoggerFactory.getLogger(TestPersonConsumer.class);
    private final String newline = "\r\n";
    private APIClient client;
    private ResourceRepositoryV2<TestPerson, Long> repoTestPerson;
    private void doRun() {
        client = new APIClient();
        repoTestPerson = client.getRepositoryForType(TestPerson.class);
        testInsert();
        testUpdate();
        testDelete();
        testFindById();
        testFindAll();
        testFindByName();
        testFindByAgeBetween();
        testFindByWageBetween();
        testFindByActive();
    }


    private void populateTable() {
        repoTestPerson.create(new TestPerson( "foo", 10, 100.0, true));
        repoTestPerson.create(new TestPerson("bar", 20, 200.0, false));
        repoTestPerson.create(new TestPerson("bim", 30, 300.0, true));
    }

    private void truncateTable() {
        List<TestPerson> all = repoTestPerson.findAll(new QuerySpec(TestPerson.class));
        all.forEach(one -> {
            repoTestPerson.delete(one.getId());
        });
    }
    private void testInsert() {
        logger.info("<<testInsert>>");
        populateTable();
        List<TestPerson> all = repoTestPerson.findAll(new QuerySpec(TestPerson.class));
        all.forEach(one-> logger.info(one.toString()));
        truncateTable();
        logger.info(newline);
    }

    private void testUpdate() {
        logger.info("<<testUpdate>>");
        populateTable();
        TestPerson one = repoTestPerson.findAll(new QuerySpec(TestPerson.class)).get(0);
        one.setName("new-foo");
        one.setAge(66);
        one.setWage(666.666);
        repoTestPerson.save(one);
        List<TestPerson> all = repoTestPerson.findAll(new QuerySpec(TestPerson.class));
        all.forEach(unos -> logger.info(unos.toString()));
        truncateTable();
        logger.info(newline);
    }

    private void testDelete() {
        logger.info("<<testDelete>>");
        populateTable();
        TestPerson one = repoTestPerson.findAll(new QuerySpec(TestPerson.class)).get(0);
        repoTestPerson.delete(one.getId());
        List<TestPerson> all = repoTestPerson.findAll(new QuerySpec(TestPerson.class));
        all.forEach(unos-> logger.info(unos.toString()));
        truncateTable();
        logger.info(newline);
    }

    private void testFindById() {
        logger.info("<<testFindById>>");
        populateTable();
        TestPerson one = repoTestPerson.findAll(new QuerySpec(TestPerson.class)).get(0);
        TestPerson anotherOne = repoTestPerson.findOne(one.getId(), new QuerySpec(TestPerson.class));
        logger.info(String.format("anotherOne = %s", anotherOne.toString()));
        truncateTable();
        logger.info(newline);
    }
    private void testFindAll() {
        logger.info("<<testFindAll>>");
        populateTable();
        List<TestPerson> all = repoTestPerson.findAll(new QuerySpec(TestPerson.class));
        all.forEach(one-> logger.info(one.toString()));
        truncateTable();
        logger.info(newline);
    }
    private void testFindByName() {
        logger.info("<<testFindByName>>");
        populateTable();
        QuerySpec querySpec = new QuerySpec(TestPerson.class);
        querySpec.addFilter(new FilterSpec(Collections.singletonList("name"), FilterOperator.EQ, "foo"));
        List<TestPerson> some = repoTestPerson.findAll(querySpec);
        some.forEach(one-> logger.info(one.toString()));
        truncateTable();
        logger.info(newline);
    }
    private void testFindByAgeBetween() {
        logger.info("<<testFindByAgeBetween>>");
        populateTable();
        QuerySpec querySpec = new QuerySpec(TestPerson.class);
        querySpec.addFilter(new FilterSpec(Collections.singletonList("minAge"), FilterOperator.EQ, new Integer(10)));
        querySpec.addFilter(new FilterSpec(Collections.singletonList("maxAge"), FilterOperator.EQ, new Integer(25)));
        List<TestPerson> some = repoTestPerson.findAll(querySpec);
        some.forEach(one-> logger.info(one.toString()));
        truncateTable();
        logger.info(newline);
    }
    private void testFindByWageBetween() {
        logger.info("<<testFindByWageBetween>>");
        populateTable();
        QuerySpec querySpec = new QuerySpec(TestPerson.class);
        querySpec.addFilter(new FilterSpec(Collections.singletonList("minWage"), FilterOperator.EQ, 100.0));
        querySpec.addFilter(new FilterSpec(Collections.singletonList("maxWage"), FilterOperator.EQ, 200.0));
        List<TestPerson> some = repoTestPerson.findAll(querySpec);
        some.forEach(one-> logger.info(one.toString()));
        truncateTable();
        logger.info(newline);
    }
    private void testFindByActive() {
        logger.info("<<testFindByActive>>");
        populateTable();
        QuerySpec querySpec = new QuerySpec(TestPerson.class);
        querySpec.addFilter(new FilterSpec(Collections.singletonList("active"), FilterOperator.EQ, true));
        List<TestPerson> some = repoTestPerson.findAll(querySpec);
        some.forEach(one-> logger.info(one.toString()));
        truncateTable();
        logger.info(newline);
    }
}
