package com.edutilos.consumer;

import com.edutilos.model.TestStudent;
import com.edutilos.model.TestTeacher;
import com.edutilos.model.TestUniversity;
import com.edutilos.util.APIClient;
import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestRelationshipConsumer {
    public static void main(String[] args) {
        TestRelationshipConsumer consumer = new TestRelationshipConsumer();
        consumer.doRun();
    }

    private final Logger logger = LoggerFactory.getLogger(TestRelationshipConsumer.class);
    private final String newline = "\r\n";
    private ResourceRepositoryV2<TestStudent, Long> testStudentRepo;
    private ResourceRepositoryV2<TestTeacher, Long> testTeacherRepo;
    private ResourceRepositoryV2<TestUniversity, Long> testUniversityRepo;
    private APIClient client;
    private void doRun() {
        client = new APIClient();
        testStudentRepo = client.getRepositoryForType(TestStudent.class);
        testTeacherRepo = client.getRepositoryForType(TestTeacher.class);
        testUniversityRepo = client.getRepositoryForType(TestUniversity.class);
        testFindById();
    }

    private void populateTables() {
        TestUniversity uniRUB = testUniversityRepo.create(new TestUniversity("RUB", "Germany", "Bochum", "44801"));
        TestUniversity uniEssen = testUniversityRepo.create(new TestUniversity("Essen Uni", "Germany", "Essen", "45141"));
        TestTeacher teacherFoo = testTeacherRepo.create(new TestTeacher("Foo", uniRUB.getId()));
        TestTeacher teacherBar = testTeacherRepo.create(new TestTeacher("Bar", uniRUB.getId()));
        TestTeacher teacherBim = testTeacherRepo.create(new TestTeacher("Bim", uniEssen.getId()));
        TestTeacher teacherPako = testTeacherRepo.create(new TestTeacher("Pako", uniEssen.getId()));
        TestStudent studentAyxan  = testStudentRepo.create(new TestStudent("Ayxan", uniRUB.getId(), Arrays.asList(teacherFoo, teacherBar).stream().collect(Collectors.toSet())));
        studentAyxan.setTestTeachers(Arrays.asList(teacherBim, teacherPako).stream().collect(Collectors.toSet()));
        studentAyxan.setUniversityId(uniEssen.getId());
        testStudentRepo.save(studentAyxan);
    }
    private void truncateTables() {
    }
    private void testFindById() {
        logger.info("<<testFindById>>");
//        populateTables();
//        TestStudent one = testStudentRepo.findOne(1L, new QuerySpec(TestStudent.class));
//        logger.info(one.toString());
        TestTeacher oneTeacher = testTeacherRepo.findOne(1L, new QuerySpec(TestTeacher.class));
        logger.info(oneTeacher.toString());
        logger.info(newline);
    }
}
