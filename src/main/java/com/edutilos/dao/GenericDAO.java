package com.edutilos.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nijat Aghayev on 22.02.19.
 */
public interface GenericDAO<T, L extends Serializable> {
    void insert(T one);
    T update(T one);
    void delete(L id);
    T findById(L id);
    List<T> findAll();
}
