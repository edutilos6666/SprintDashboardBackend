package com.edutilos.dao;

import com.edutilos.model.TestStudent;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestStudentDAO implements GenericDAO<TestStudent, Long> {
    @PersistenceContext(unitName = "sprint_dashboard")
    private EntityManager em;


    @Transactional
    @Override
    public void insert(TestStudent one) {
        em.persist(one);
    }

    @Transactional
    @Override
    public TestStudent update(TestStudent one) {
        return em.merge(one);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        TestStudent one = findById(id);
        em.remove(one);
    }

    @Override
    public TestStudent findById(Long id) {
        return em.find(TestStudent.class, id);
    }

    @Override
    public List<TestStudent> findAll() {
        return em.createQuery("from TestStudent", TestStudent.class)
                .getResultList();
    }
}
