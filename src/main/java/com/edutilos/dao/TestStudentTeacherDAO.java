package com.edutilos.dao;

import com.edutilos.model.TestStudentTeacher;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestStudentTeacherDAO implements GenericDAO<TestStudentTeacher, Long> {
    @PersistenceContext(unitName = "sprint_dashboard")
    private EntityManager em;

    @Transactional
    @Override
    public void insert(TestStudentTeacher one) {
        em.persist(one);
    }

    @Transactional
    @Override
    public TestStudentTeacher update(TestStudentTeacher one) {
        return em.merge(one);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        TestStudentTeacher one = findById(id);
        em.remove(one);
    }

    @Transactional
    public int deleteByStudentId(Long studentId) {
        return em.createQuery("delete from TestStudentTeacher where studentId = :studentId", TestStudentTeacher.class)
                .setParameter("studentId", studentId)
                .executeUpdate();
    }

    @Transactional
    public int deleteByTeacherId(Long teacherId) {
        return em.createQuery("delete from TestStudentTeacher where teacherId = :teacherId", TestStudentTeacher.class)
                .setParameter("teacherId", teacherId)
                .executeUpdate();
    }


    @Override
    public TestStudentTeacher findById(Long id) {
        return em.find(TestStudentTeacher.class, id);
    }

    @Override
    public List<TestStudentTeacher> findAll() {
        return em.createQuery("from TestStudentTeacher", TestStudentTeacher.class)
                .getResultList();
    }

    public List<TestStudentTeacher> findByStudentId(Long studentId) {
        return em.createQuery("from TestStudentTeacher where studentId = :studentId", TestStudentTeacher.class)
                .setParameter("studentId", studentId)
                .getResultList();
    }

    public List<TestStudentTeacher> findByTeacherId(Long teacherId) {
        return em.createQuery("from TestStudentTeacher where teacherId = :teacherId", TestStudentTeacher.class)
                .setParameter("teacherId", teacherId)
                .getResultList();
    }
}
