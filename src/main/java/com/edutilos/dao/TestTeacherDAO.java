package com.edutilos.dao;

import com.edutilos.model.TestTeacher;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestTeacherDAO implements GenericDAO<TestTeacher, Long> {
    @PersistenceContext(unitName = "sprint_dashboard")
    private EntityManager em;

    @Transactional
    @Override
    public void insert(TestTeacher one) {
        em.persist(one);
    }

    @Transactional
    @Override
    public TestTeacher update(TestTeacher one) {
        return em.merge(one);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        TestTeacher one = findById(id);
        em.remove(one);
    }

    @Override
    public TestTeacher findById(Long id) {
        return em.find(TestTeacher.class, id);
    }

    @Override
    public List<TestTeacher> findAll() {
        return em.createQuery("from TestTeacher", TestTeacher.class)
                .getResultList();
    }
}
