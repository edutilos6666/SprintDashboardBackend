package com.edutilos.dao;

import com.edutilos.model.TestPerson;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Nijat Aghayev on 22.02.19.
 */
public class TestPersonDAO implements GenericDAO<TestPerson, Long> {
    @PersistenceContext(unitName = "sprint_dashboard")
    private EntityManager em;


    @Transactional
    @Override
    public void insert(TestPerson one) {
        em.persist(one);
    }

    @Transactional
    @Override
    public TestPerson update(TestPerson one) {
        return em.merge(one);
    }


    @Transactional
    @Override
    public void delete(Long id) {
        TestPerson one = findById(id);
        em.remove(one);
    }

    @Override
    public TestPerson findById(Long id) {
        return em.find(TestPerson.class, id);
    }

    @Override
    public List<TestPerson> findAll() {
        return em.createQuery("from TestPerson", TestPerson.class)
                .getResultList();
    }

    public List<TestPerson> findByName(String name) {
        return em.createQuery("from TestPerson where name = :name", TestPerson.class)
                .setParameter("name", name)
                .getResultList();
    }

    public List<TestPerson> findByAgeBetween(Integer minAge, Integer maxAge, boolean inclusive) {
        String sql = "from TestPerson where age >= :minAge and age <= :maxAge";
        if(!inclusive)
            sql = "from TestPerson where age > :minAge and age < :maxAge";
        return em.createQuery(sql, TestPerson.class)
                .setParameter("minAge", minAge)
                .setParameter("maxAge", maxAge)
                .getResultList();
    }

    public List<TestPerson> findByWageBetween(Double minWage, double maxWage, boolean inclusive) {
        String sql = "from TestPerson where wage >= :minWage and wage <= :maxWage";
        if(!inclusive)
            sql = "from TestPerson where wage > :minWage and wage < :maxWage";
        return em.createQuery(sql, TestPerson.class)
                .setParameter("minWage", minWage)
                .setParameter("maxWage", maxWage)
                .getResultList();
    }

    public List<TestPerson> findByActive(boolean active) {
        return em.createQuery("from TestPerson where active = :active", TestPerson.class)
                .setParameter("active", active)
                .getResultList();
    }
}
