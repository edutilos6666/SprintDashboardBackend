package com.edutilos.dao;

import com.edutilos.model.TestUniversity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Nijat Aghayev on 24.02.19.
 */
public class TestUniversityDAO implements GenericDAO<TestUniversity, Long> {
    @PersistenceContext(unitName = "sprint_dashboard")
    private EntityManager em;

    @Transactional
    @Override
    public void insert(TestUniversity one) {
        em.persist(one);
    }

    @Transactional
    @Override
    public TestUniversity update(TestUniversity one) {
        return em.merge(one);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        TestUniversity one = findById(id);
        em.remove(one);
    }

    @Override
    public TestUniversity findById(Long id) {
        return em.find(TestUniversity.class, id);
    }

    @Override
    public List<TestUniversity> findAll() {
        return em.createQuery("from TestUniversity", TestUniversity.class)
                .getResultList();
    }
}
