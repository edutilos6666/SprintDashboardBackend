-- mysql -uroot -proot sprint_dashboard < mysql_tables.sql
drop table test_student;
drop table test_teacher; 
drop table test_university;
drop table test_student_teacher;

create table test_student (
 ID bigint primary key auto_increment, 
 NAME varchar(50), 
 UNIVERSITY_ID bigint not null
);

create table test_teacher (
  ID bigint primary key auto_increment, 
  NAME varchar(50), 
  UNIVERSITY_ID bigint not null
);


create table test_university (
  ID bigint primary key auto_increment, 
  NAME varchar(50), 
  COUNTRY varchar(50), 
  CITY varchar(50), 
  PLZ varchar(50)
);

create table test_student_teacher (
  ID bigint primary key auto_increment, 
  STUDENT_ID bigint not null,
  TEACHER_ID bigint not null
);







